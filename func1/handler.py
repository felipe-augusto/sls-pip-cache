from langdetect import detect

def process(event, context):
  payload = json.loads(event["body"])

  if 'text' not in payload:
      print {
          "statusCode": 400,
          "body": {
              "error": "You should have passed the text you want to process"
          }
      }

  text = payload["text"].replace('\n', ' ').replace('\r', '')
  return detect(text)