try:
  import unzip_requirements
except ImportError:
  pass

import handler
import os

def main(event, context):
  return handler.process(event, context)